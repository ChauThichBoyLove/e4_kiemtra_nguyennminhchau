﻿namespace KiemmTra_NMC.Models
{
    public class Logs
    {
        public int Id { get; set; }
        public int TransactionalId { get; set; }
        public Transactions? Transactions { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public List<Reports> Reports { get; set; }
    }
}
