﻿namespace KiemmTra_NMC.Models
{
    public class Employees
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Transactions> Transactions { get; set; }
    }
}
