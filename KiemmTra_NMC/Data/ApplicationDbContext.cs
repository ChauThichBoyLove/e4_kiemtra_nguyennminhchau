﻿using KiemmTra_NMC.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KiemmTra_NMC.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        
        


    }
}
