﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KiemmTra_NMC.Migrations
{
    /// <inheritdoc />
    public partial class okkk : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogsID",
                table: "Reports");

            migrationBuilder.RenameColumn(
                name: "LogsID",
                table: "Reports",
                newName: "LogsId");

            migrationBuilder.RenameIndex(
                name: "IX_Reports_LogsID",
                table: "Reports",
                newName: "IX_Reports_LogsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogsId",
                table: "Reports",
                column: "LogsId",
                principalTable: "Logs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogsId",
                table: "Reports");

            migrationBuilder.RenameColumn(
                name: "LogsId",
                table: "Reports",
                newName: "LogsID");

            migrationBuilder.RenameIndex(
                name: "IX_Reports_LogsId",
                table: "Reports",
                newName: "IX_Reports_LogsID");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogsID",
                table: "Reports",
                column: "LogsID",
                principalTable: "Logs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
