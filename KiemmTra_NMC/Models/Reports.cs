﻿namespace KiemmTra_NMC.Models
{
    public class Reports
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsId { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionalId { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
